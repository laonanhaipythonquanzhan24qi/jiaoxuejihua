import socket

phone = socket.socket()

phone.connect(('127.0.0.1',8848))

to_server_data = input('>>>').strip().encode('utf-8')
phone.send(to_server_data)

from_server_data = phone.recv(1024)  # 最多接受1024字节
print(f'来自服务端消息:{from_server_data.decode("utf-8")}')

phone.close()

