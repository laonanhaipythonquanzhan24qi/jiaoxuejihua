# 在python中引号引起来就是字符串
# 字符串是用来存储少量数据
# name = "meat"
# meat 每一个字母叫做一个元素
      # 0123       从左向右
      #-4-3-2-1    从右向左
      # 索引(下标) 通过索引可以精确的定位到某个元素
# print(name[-1])

# name = "今天是个好日子"
       # 0 1 2 3 4 5 6
       #-7-6-5-4-3-2-1
# a = name[0]
# b = name[1]
# print(a+b)
# 切片
# print(name[0:2]) # 顾头不顾尾  name[起始位置:终止位置]
# print(name[:]) # 某个位置不指定的时候默认取最后或最前
# print(name[2:5])
# print(name[-2:-5])

# print(name[-2:-5:-1]) # [起始位置:终止位置:步长] 步长默认为1

name = "大黑哥吃大煎饼"
# print(name[1:5])
# print(name[-2:-6:-1])
# print(name[-6:6])

# a = name[0]
# b = name[2]
# c = name[4]
# d = name[6]
# print(a+b+c+d)
# print(name[::2])

# print(name[100:105]) #切片的时候起始位置和终止位置都超出的时候不会进行报错
# print(name[100])  # 索引的时候索引值超出范围的时候回报错

"""
s = 'Python最NB'
获取s字符串中前3个内容
s[0:3]
获取s字符串中第3个内容
s[2]
获取s字符串中后3个内容
s[-3:]
获取s字符串中第3个到第8个
s[2:8]
获取s字符串中第2个到最后一个
s[1:]
获取s字符串中第1,3,5个内容
s[0:6:2]
获取s字符串中第2,4,6个内容
s[1:7:2]
获取s字符串中所有内容
s[:]
获取s字符串中第4个到最后一个,每2个取一个
s[3::2]
获取s字符串中倒数第5个到最开始,每3个取一个
s[-5::-3]　
"""
# s = 'Python最NB'
# print(s[-5::-3])

# 字符串的方法:
s = "ALEX"
# s1 = s.upper() #全部大写
# print(s1)

# s1 = s.lower() # 全部小写
# print(s1)

# 应用场景
# s = input("验证码(AbC5)")
# if s.upper() == "AbC5".upper():
#     print("验证码正确")
# else:
#     print("验证码错误!")

# 以什么开头:
# s = "ALEX"
# s1 = s.startswith("E",2,6)
# print(s1)

# 以什么结尾:
# s = "ALEX"
# s1 = s.endswith("X",3,4)
# print(s1)

# 统计
# s = "alexdxjbx"
# s1 = s.count("x")
# print(s1)

# 脱: 字符串头尾两端的空格和换行符以及制表符
# n = input(">>>")
# if n.strip() == "alex":
#     print("1")
# else:
#     print("2")

# s = "alexdsba"
# s1 = s.strip("a") # 可以指定内容取脱
# print(s1)


# 分割:以空格和换行符以及制表符进行分割
# s = "aelxlaaa"
# s1 = s.split("l",maxsplit=1)  # 可以通过指定方式进行切割
# print(s1)


# 替换:
# s = "大黑哥吃肉夹馍,肉夹馍"
# s1 = s.replace("肉夹馍","大煎饼")
# s1 = s.replace("肉夹馍","大煎饼",1) # 指定替换的次数
# print(s1)

# is 系列:
# s = "12.3"

# print(s.isalnum()) # 判断是不是字母,数字,中文
# print(s.isalpha())  # 判断是不是字母,中文
# print(s.isdigit())  # 判断字符串是不是全都是阿拉伯数字
# print(s.isdecimal())  # 判断是否是十进制


# print(len(name))

"""
你
好
啊
"""

# count = 0
# while count < len(name):
#     print(name[count])
#     count += 1

# name = "你好啊"
# for x in name:  # for循环
#     print(x)

# name = "你好啊"
# for i in name:  # for循环
#     print(i)

# i = "你"
# print(i)
# i = "好"
# print(i)
# i = "啊"
# print(i)
#
#
# print(3)
# print(54)
# print(67)
# print(i)



# for 关键字
# i 是变量名
# in 关键字
# name 可迭代对象

# name = "alex"
# for x in name:
#     print(x)
# print(x)


# 错误的示范
# name = True
# for i in name:
#     print(i)

# 数据类型中只有整型和布尔值不能够进行for循环