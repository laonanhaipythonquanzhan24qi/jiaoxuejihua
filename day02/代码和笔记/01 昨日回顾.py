# 1.python的历史
#     2004 Django框架
#     python2 和 python3的区别
#         python2 源码不统一 有重复功能代码
#         python3 源码统一   没有重复功能代码

# 基本数据类型
#     int --  整型（数字用于比较大小和计算）
#     str --  字符串（字符串 + 都是字符串相加就是拼接/字符串和数字相乘/Python中只要是用引号引起来就是字符串）
#     bool -- 布尔值（判断真假）


# pyhton是什么编程语言
#     python的解释型语言


# python的种类
#     cpyhton jython ironpython pypy

# 变量
# a = 1
# a 变量名
# = 赋值
# 1 值

# 在声明变量的时候，执行顺序是从右向左执行
# 在一块内存中同名的变量名只能有一个



# 常量：
    # 变量名全部大写就是常量 -- 常量在配置文件中使用 --不建议修改


# 注释：
    # ctrl + ？ 所选择的内容被注释
"""
常鑫
"""
'''
大黑哥
'''
"""''' 111  '''"""
    # 单行注释(当行注释)
    # 被注释的内容前加一个#号
    # 被注释的内容不会被执行

# 用户交互：
# a = input("提示语句") 当执行到input的时候，程序就会卡住了
# input获取到的内容全都是字符串


# 流程控制语句：
    # if 条件:
    #     结果
    #
    # if else
    #
    # if elif elif
    #
    # if elif elif else
    #
    # if if if
    #
    # if 条件：
    #     if 条件：


# 其他知识：
# type 查看类型
# int("5")  --> 5
# str(5) --> "5"
# == 判断两个内容是否相同
# and 和，and前后都为真才是真