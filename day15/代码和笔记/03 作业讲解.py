"""
将模拟博客园使用装饰器进行编写
"""


login_dic = {
    "username": None,
    "flag": False,
    "count":3
}


# def auth(f):
#     def inner(*args,**kwargs):
#         if login_dic["flag"]:
#             f()
#         else:
#             login(f)  #f ==  comment
#     return inner
#
# @auth
# def index():
#     print(f"这是{login_dic['username']}主页")
#     return "主页没有内容"
#
# def login(func=False): # func = comment
#     while login_dic["count"]:
#         print("这是一个登录页面")
#         user = input("username:")
#         pwd = input("password:")
#         if user == "baoyuan" and pwd == "baoyuan123":
#             login_dic["flag"] = True
#             login_dic["username"] = user
#             login_dic["count"] = 0
#             print("登录成功!")
#             if func:
#                 func()
#
#         else:
#             login_dic["count"] -= 1
#             print(f"用户名或密码错误!剩余次数{login_dic['count']}")
#
# @auth
# def comment():
#     print(f"这是{login_dic['username']}评论")

# comment()
# login()

# comment()

"""
请实现一个装饰器，每次调用函数时，将被装饰的函数名以及调用被装饰函数的时间节点写入文件中。

可用代码：
import time
struct_time = time.localtime()
print(time.strftime("%Y-%m-%d %H:%M:%S",struct_time)) # 获取当前时间节点
   
def func():
    print(func.__name__)
函数名通过： 函数名.__name__获取。
"""
# import time
# def wrapper(func):
#     def inner(*args,**kwargs):
#         func_name = func.__name__
#         struct_time = time.localtime()
#         str_time = time.strftime("%Y-%m-%d %H:%M:%S", struct_time)
#         with open("userinfo","a",encoding="utf-8")as f:
#             f.write(f"函数名:{func_name},时间:{str_time}\n")
#         func(*args,**kwargs)
#     return inner
#
# @wrapper
# def foo():
#     print("这是一个被装饰的函数")
#
# @wrapper
# def f1():
#     print("这是一个懵逼的李业")
# f1()