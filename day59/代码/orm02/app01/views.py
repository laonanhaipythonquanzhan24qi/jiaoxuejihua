from django.shortcuts import render,HttpResponse
from app01 import models
# Create your views here.
# 1 models文件中创建一个类

# class UserInfo(models.Model):
#     id = models.AutoField(primary_key=True)
#     name = models.CharField(max_length=10)
#     bday = models.DateField()
#     checked = models.BooleanField()
# 2 执行数据库同步指令
# python manage.py makemigrations
# python manage.py migrate


def query(request):
    # 创建一条记录,增
    import datetime
    current_date = datetime.datetime.now()
    # new_obj = models.UserInfo(
    #     id=2,
    #     name='子文',
    #     bday='2019-09-27',  #current_date
    #     checked=1,
    #
    # )
    # new_obj.save()  #翻译成sql语句,然后调用pymysql,发送给服务端  insert into app01_userinfo values(2,'子文','2019-09-27',1)


    models.UserInfo.objects.create(
        name='杨泽涛3',
        bday=current_date,
        # now=current_date,
        # now2=current_date,
        checked=0

    )
    # delelte from app01_userinfo where id = 7
    # print(ret)  #UserInfo object  卫贺
    # print(ret.name)  #UserInfo object
    # print(ret.bday)  #UserInfo object

    # models.UserInfo.objects.filter(id=7).delete()
    # # ret.delete()
    #
    # list() -- []  [0]
    # QuerySet() -- <QuerySet [<UserInfo: 卫贺>,<UserInfo: 卫贺2><UserInfo: 卫贺3>]> .delete()
    # # print(ret)  #<QuerySet [<UserInfo: 卫贺4>]> -- [<UserInfo: 卫贺4>]
    # < UserInfo: 卫贺 > -- UserInfo()

    # update app01_userinfo set name='渣渣文' where id=2;
    # 方式1:
    # models.UserInfo.objects.filter(id=9).update(
    #     name='回首掏',
    #     checked = 0,
    #
    # )
    # 错误示例,model对象不能调用update方法
    # models.UserInfo.objects.filter(id=2)[0].update(
    #     name='加篮子+2',
    #     # checked = 0,
    # )
    # 方式2
    # ret = models.UserInfo.objects.filter(id=9)[0]
    # ret.name = '回首掏,走位走位'
    # ret.save()

    # print(ret)
    return HttpResponse('xxx')


