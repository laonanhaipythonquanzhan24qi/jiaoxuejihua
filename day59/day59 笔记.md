

# 昨日内容回顾

## 模板相关

### 模板继承(母版继承)

```
1. 创建一个xx.html页面(作为母版,其他页面来继承它使用)
2. 在母版中定义block块(可以定义多个,整个页面任意位置)
	{% block content %}  <!-- 预留的钩子,共其他需要继承它的html,自定义自己的内容 -->

	{% endblock %}


3 其他页面继承写法
	{% extends 'base.html' %}  必须放在页面开头
4 页面中写和母版中名字相同的block块,从而来显示自定义的内容
    {% block content %}  <!-- 预留的钩子,共其他需要继承它的html,自定义自己的内容 -->
        {{ block.super }}  #这是显示继承的母版中的content这个快中的内容
        这是xx1
    {% endblock %}

```



### 组件

```
1 创建html页面,里面写上自己封装的组件内容,xx.html
2 新的html页面使用这个组件
	{% include 'xx.html' %}
```

### 自定义标签和过滤器

```
1 在应用下创建一个叫做templatetags的文件夹(名称不能改),在里面创建一个py文件,例如xx.py

2 在xx.py文件中引用django提供的template类,写法
	from django import template
	register = template.Library() #register变量名称不能改
	
	定义过滤器
		@register.filter   参数至多两个
		def xx(v1,v2):
			return xxx
	使用:
		{% load xx %}
		{{ name|xx:'oo' }}
	
	# 自定义标签 没有参数个数限制
    @register.simple_tag
    def huxtag(n1,n2):  #冯强xx  '牛欢喜'
        '''
        :param n1:  变量的值 管道前面的
        :param n2:  传的参数 管道后面的,如果不需要传参,就不要添加这个参数
        :return:
        '''
        return n1+n2

    # inclusion_tag 返回html片段的标签
    @register.inclusion_tag('result.html')
    def res(n1): #n1 : ['aa','bb','cc']

        return {'li':n1 }
	使用:
		{% res a %}
	
```



### 静态文件配置

```
1 项目目录下创建一个文件夹,例如名为jingtaiwenjianjia,将所有静态文件放到这个文件夹中
2 settings配置文件中进行下面的配置
	# 静态文件相关配置
    STATIC_URL = '/abc/'  #静态文件路径别名

    STATICFILES_DIRS = [
        os.path.join(BASE_DIR, 'jingtaiwenjianjia'),
    ]

3 引入<link rel="stylesheet" href="/abc/css/index.css">

```

### url别名和反向解析

```
写法
	url(r'^index2/', views.index,name='index'),
	url(r'^index2/(\d+)/', views.index,name='index'),
反向解析
	后端: from django.urls import reverse
		 reverse('别名')  例如:reverse('index') -- /index2/
		 带参数的反向解析:reverse('index',args=(10,11,))	-- /index2/10/
	html: {% url '别名' %} -- 例如:{% url 'index' %} -- /index2/
		 带参数的反向解析:{% url '别名' 参数1 参数2 %} 例如:{% url 'index' 10 %} -- /index2/10/  <a href='/index2/10/'>hhh</a>
	
	
```

### url命名空间

路由分发 include

```
1 在每个app下创建urls.py文件,写上自己app的路径
2 在项目目录下的urls.py文件中做一下路径分发,看下面内容
    from django.conf.urls import url,include
    from django.contrib import admin

    urlpatterns = [
        # url(r'^admin/', admin.site.urls),
        url(r'^app01/', include('app01.urls')),#app01/home/
        url(r'^app02/', include('app02.urls')),
    ]

```

命名空间namespace

```
from django.conf.urls import url,include
from django.contrib import admin
urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^app01/', 	include('app01.urls',namespace='app01')),#app01/home/
    url(r'^app02/', include('app02.urls',namespace='app02')),
	
]


使用:
	后端:reverse('命名空间名称:别名') -- reverse('app01:home') 
	hmtl:{% url '命名空间名称:别名' %}  -- {% url 'app01:home' %}
```



# 今日内容

## orm单表操作  对象关系映射(object relational mapping)

orm语句 -- sql -- 调用pymysql客户端发送sql -- mysql服务端接收到指令并执行

### orm介绍

```
django 连接mysql
1 settings配置文件中
	DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'orm02',
            'USER':'root',
            'PASSWORD':'666',
            'HOST':'127.0.0.1',
            'PORT':3306,
        }
    }
2 项目文件夹下的init文件中写上下面内容,用pymysql替换mysqldb
	import pymysql
	pymysql.install_as_MySQLdb()

3 models文件中创建一个类

# class UserInfo(models.Model):
#     id = models.AutoField(primary_key=True)
#     name = models.CharField(max_length=10)
#     bday = models.DateField()
#     checked = models.BooleanField()
4 执行数据库同步指令,添加字段的时候别忘了,该字段不能为空,所有要么给默认值,要么设置它允许为空 null=True
# python manage.py makemigrations
# python manage.py migrate

5 创建记录(实例一个对象,调用save方法)
def query(request):
    # 创建一条记录,增

    new_obj = models.UserInfo(
        id=2,
        name='子文',
        bday='2019-09-27',
        checked=1,

    )
    new_obj.save()  #翻译成sql语句,然后调用pymysql,发送给服务端  insert into app01_userinfo values(2,'子文','2019-09-27',1)

    return HttpResponse('xxx')
```



增:

```
方式1:
    new_obj = models.UserInfo(
        id=2,
        name='子文',
        bday='2019-09-27',
        checked=1,

    )
    new_obj.save() 
方式2:
	# ret 是创建的新的记录的model对象(重点)
	ret = models.UserInfo.objects.create(
        name='卫贺',
        bday='2019-08-07',
        checked=0
    )

    print(ret)  #UserInfo object  卫贺
    print(ret.name)  #UserInfo object
    print(ret.bday)  #UserInfo object


```

### 时间问题

```
models.UserInfo.objects.create(
        name='杨泽涛2',
        bday=current_date,
        # now=current_date,  直接插入时间没有时区问题
        checked=0
    )
	但是如果让这个字段自动来插入时间,就会有时区的问题,auto_now_add创建记录时自动添加当前创建记录时的时间,存在时区问题
now = models.DateTimeField(auto_now_add=True,null=True)
解决方法:
    settings配置文件中将USE_TZ的值改为False
    # USE_TZ = True
    USE_TZ = False  # 告诉mysql存储时间时按照当地时间来寸,不要用utc时间
使用pycharm的数据库客户端的时候,时区问题要注意
```



删

```
简单查询:filter()  -- 结果是queryset类型的数据里面是一个个的model对象,类似于列表
	models.UserInfo.objects.filter(id=7).delete()  #queryset对象调用
	models.UserInfo.objects.filter(id=7)[0].delete()  #model对象调用
```

改

```
方式1:update
    # models.UserInfo.objects.filter(id=2).update(
    #     name='篮子文',
    #     checked = 0,
    #
    # )
    # 错误示例,model对象不能调用update方法
    # models.UserInfo.objects.filter(id=2)[0].update(
    #     name='加篮子+2',
    #     # checked = 0,
    # )
方式2 
    ret = models.UserInfo.objects.filter(id=2)[0]
    ret.name = '加篮子+2'
    ret.checked = 1
    ret.save()
    

更新时的auto_now参数
	# 更新记录时,自动更新时间,创建新纪录时也会帮你自动添加创建时的时间,但是在更新时只有使用save方法的方式2的形式更新才能自动更新时间,有缺陷,放弃
    now2 = models.DateTimeField(auto_now=True,null=True)
```





























































































































