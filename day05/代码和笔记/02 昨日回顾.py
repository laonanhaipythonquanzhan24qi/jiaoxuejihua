# 1.列表 -- list
# 增
#   append
#   insert
#   extend
# 删
#   del
#   pop
#   remove
#   clear
# 改
#   lst[0] = 12
#   lst[1:4] = 1,2,2,34,5,6,7
#   lst[1:4:2] = 1,2 必须一一对应
# 查
# for i in lst:
#     print(i)

# 2.元组 -- tuple
# 就是一个不可变得列表

# 3.元组和列表 -- 支持索引,切片,步长 -- 嵌套

# 4.range -- 范围
# range(10) -- 起始位置:0 -- 终止位置:9  (顾头不顾尾)\
# range(1,10) -- 起始位置:1 - 终止位置:9:默认步长:1
# range(10,1) -- 起始:10 -- 终止:1: 默认步长1
# range(10,1,-1) -- 起始:10 -- 终止:2 步长:-1 从右向左查找


# python3 range() 获取的就是自己本身
# pyhton2 range() 获取的就是列表

# 其他:
# pyhton2 xrange  python3 range相似
# 面试题:
