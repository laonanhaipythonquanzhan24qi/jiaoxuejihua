# 昨日内容回顾

## HTTP协议

```
超文本传输协议(应用层的,基于tcp\ip协议的)

特点:无状态,无连接(短连接)

请求消息格式:请求行\请求头部\空行\请求数据
	请求行:GET 路径 HTTP/1.1
	
响应消息格式:响应行\响应头部\空行\响应数据
	响应行: HTTP/1.1 状态码 状态描述

请求方法:get\post\put\delete\head\options\trace\connect\

get请求和post的请求的区别
	get,数据放在url上,有长度限制,一般用于获取数据
	post请求,数据放在请求数据部分,没有长度限制,一般用于提交数据

状态码:
	1xx请求被接受但是还在处理当中 
	2xx请求成功
	3xx重定向(301永久重定向和302临时重定向)
	4xx客户端错误(请求错误)
	5xx服务器错误

```



## MVC和MTV

```
MVC模式
    M -- model数据库相关
    V -- views视图(逻辑相关)
    C -- controller url控制器(url分发器)
django -- MTV模式
	M -- model数据库相关
	T -- templates HTML相关(模板)
	V -- views视图(逻辑相关)
	+ url分发器
```



## django安装使用

```
安装:pip install django==1.11.9
创建项目: django-admin startproject 项目名
启动项目: python manage.py runserver 127.0.0.1:8001
创建应用: python manage.py startapp app名称
	指令创建的app需要配置
	项目的settings配置文件中,installapps的列表,添加一个app名称作为配置
	pycharm中创建完app之后,如果在想创建新的app,需要执行创建app的命令,并添加配置

```



## url路由

```
from app01 import views
urlpatterns = [
	url(r'^index/',views.index)
	url(r'^index/(\d+)/(\d+)/',views.index),无名分组,分组数据以位置传参的方式给了视图函数
	url(r'^index/(?P<xx>\d+)/(?P<oo>\d+)/',views.index),有名分组,分组数据以关键字传参的方式给了视图函数,不在乎参数位置了,并且视图函数形参的名称要和又名分组的变量名称一样.
]


views.py
def index(request,n,m):
	return HttpResponse('xx')

```



# 今日内容

## 视图

HTTPRequest对象就是咱们的视图函数的参数request

```

    # print(request)  #<WSGIRequest: GET '/home/'>
    # # print(dir(request))
    #
    # print(request.path) #/home/  纯路径
    # print(request.path_info) #/home/  纯路径
    # print(request.get_full_path()) #/home/?a=1&b=2  全路径(不包含ip地址和端口)

    # print(request.META)  #请求头相关数据,是一个字典

    # print(request.method)  #GET

    # print(request.GET)
    
    # print(request.POST)
    # print(request.body)  能够拿到请求数据部分的数据(post,get没有)

```

HTTPResponse对象

```
HTTPResponse('字符串')
render(request,'xx.html')

redirect 重定向#用法  redirect(路径) 示例:redirect('/index/')

```



FBV和CBV 视图(视图函数和视图类)



类视图 CBV

views.py

```views.py

from django.views import View
class LoginView(View):

    # def dispatch(self, request, *args, **kwargs):
    #     print('xx请求来啦!!!!')
    #     ret = super().dispatch(request, *args, **kwargs)
    #     print('请求处理的逻辑已经结束啦!!!')
    #     return ret
    def get(self,request):  #处理get请求直接定义get方法,不需要自己判断请求方法了,源码中用dispatch方法中使用了反射来处理的
        print('小小小小')
        return render(request,'login.html')

    def post(self,request):
        print(request.POST)
        return HttpResponse('登录成功')
```

urls.py路由写法

```
url(r'^login/', views.LoginView.as_view()),
```



### 视图加装饰器

```

视图函数
	def wrapper(func):
        def inner(*args, **kwargs):
            print(11111)
            ret = func(*args, **kwargs)
            print(22222)
            return ret
        return inner


    @wrapper
    def index(request):
        print('xxxxx')
        return HttpResponse('indexxxxxxxxx')

视图类
	from django.utils.decorator import method_decorator
    @method_decorator(wrapper,name='get')  # 方式3
    class LoginView(View):
        # @method_decorator(wrapper) #方式2
        # def dispatch(self, request, *args, **kwargs):
        #     print('xx请求来啦!!!!')
        #
        #     ret = super().dispatch(request, *args, **kwargs)
        #
        #     print('请求处理的逻辑已经结束啦!!!')
        #     return ret
        # @method_decorator(wrapper)  #方式1
        def get(self,request):
            print('小小小小')
            return render(request,'login.html')

        def post(self,request):
            print(request.POST)
            return HttpResponse('登录成功')

```



## 模板渲染

```
{{ 变量 }}   {% 逻辑 %} -- 标签
```

### 万能的点

```
<h1>91李业网</h1>
<h2>{{ name }}</h2>
<h2>{{ d1.items }}</h2>
<h2>我是"{{ l1.1 }}"</h2>
<h2>{{ num }}</h2>
<h2>{{ obj.p }}</h2>  #如果调用的方法需要传参,sorry用不了
```

### 过滤器(内置)

```
参考博客:https://www.cnblogs.com/clschao/articles/10414811.html
```

### 标签

#### for循环标签

```
循环列表等
{% for person in person_list %}
    <p>{{ person.name }}</p>  <!--凡是变量都要用两个大括号括起来-->
{% endfor %}
循环字典
{% for key,val in dic.items %}
    <p>{{ key }}:{{ val }}</p>
{% endfor %}

empty
{% for person in person_list %}
    <p>{{ person.name }}</p>  <!--凡是变量都要用两个大括号括起来-->
{% empty %}
	<p>没有找到东西!</p>
{% endfor %}


forloop.counter            当前循环的索引值(从1开始)，forloop是循环器，通过点来使用功能
forloop.counter0           当前循环的索引值（从0开始）
forloop.revcounter         当前循环的倒序索引值（从1开始）
forloop.revcounter0        当前循环的倒序索引值（从0开始）
forloop.first              当前循环是不是第一次循环（布尔值）
forloop.last               当前循环是不是最后一次循环（布尔值）
forloop.parentloop         本层循环的外层循环的对象，再通过上面的几个属性来显示外层循环的计数等
示例:
	{% for i in d2 %}
        {% for k,v in d1.items %}

            <li>{{ forloop.counter }}-- {{ forloop.parentloop.counter }} === {{ k }} -- {{ v }}</li>

        {% endfor %}

    {% endfor %}

```

if判断标签

```
{% if num > 100 or num < 0 %}
    <p>无效</p>  <!--不满足条件，不会生成这个标签-->
{% elif num > 80 and num < 100 %}
    <p>优秀</p>
{% else %}  <!--也是在if标签结构里面的-->
    <p>凑活吧</p>
{% endif %}

if语句支持 and 、or、==、>、<、!=、<=、>=、in、not in、is、is not判断，注意条件两边都有空格。
```



with

```
方法1
{% with total=business.employees.count %}  #注意等号两边不能有空格
    {{ total }} <!--只能在with语句体内用-->
{% endwith %}
方法2
{% with business.employees.count as total %}
    {{ total }}
{% endwith %}

```



























































