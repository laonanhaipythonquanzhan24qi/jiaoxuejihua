# str: 不可变
# 1.1 首字母大写:
# name = "alex"
# name1 = name.capitalize()
# print(name1)

# 1.2 每个单词首字母大写
# name = "alex wusir"
# print(name.title())

# 1.3 大小写反转
# name = "Alex"
# print(name.swapcase())

# 1.4 居中 -- 填充
# name = "alex"
# print(name.center(20,"-"))

# 1.5 查找 从左向右 只查找一个
# name = "alelx"
# print(name.find("b"))  #find查找不存在的返回-1
# print(name.index("b"))   #index查找不存在的就报错

# 1.6 拼接
# name = "al3x"
# print("_".join(name))  ***

# 1.7 格式化
# 1.%s
# 2.f
# 3.name.format()
# name = "alex{},{},{}"
# print(name.format(1,2,3)) # 按照顺序位置进行填充
# name = "alex{2},{0},{1}"
# print(name.format("a","b","c")) # 按照索引值进行填充
# name = "alex{a},{b},{c}"
# print(name.format(a=1,c=11,b=67)) # 按照关键字进行填充

# + * 开辟新的空间

# name = "alex"
# name1 = "wusir"
# print(id(name))
# print(id(name1))
# print(id(name + name1))

# list:
# 定义方式:
    # list("123")
# 其他方法:

# lst = [1,2,23,234,435,36,23,213421,421,4231,534,65]
# lst.sort()  # 排序 (升序)
# print(lst)

# lst = ["你好","我好"]
# lst.sort()  # 排序 (默认升序)
# print(lst)

# lst.sort(reverse=True) # 降序
# print(lst)

# lst = [1,2,3,4453,5,6,7]
# print(lst[::-1])
# lst.reverse()  # 反转
# print(lst)

# lst = [1,2,3,4,5123,21345,231123,4,1235,234,123]
# lst.sort()
# lst.reverse()
# print(lst)

#面试题:
# lst = [[]]
# new_lst = lst * 5
# new_lst[0].append(10)
# print(new_lst)

# lst = [1,[]]
# new_lst = lst * 5
# new_lst[0] = 10
# print(new_lst)

# lst = [1,[]]
# new_lst = lst * 5
# new_lst[1] = 10
# print(new_lst)

# 方式一:
# lst.extend(lst1)
# print(lst)
# 方式二:
# print(lst+lst1)
# new_lst = lst * 5
# print(id(new_lst[0]), id(new_lst[0]))

# lst = [[]]
# new_lst = lst * 5
# new_lst[0].append(10)
# print(new_lst)

# tuple:
# tu = ("12")  # 数据类型是()中数据本身
# print(type(tu))

# tu = (1,)   # (1,)是元组
# print(type(tu))

# 元组 + * 不可变共用,可变也共用

# dict:
# 定义一个字典:
    # print(dict(k=1,k1=2))

# 随机删除: popitem
# dic = {"key":1,"key2":2,"key3":56}
# print(dic.popitem())  # 返回的是被删除的键值对(键,值)
# print(dic)
# python36 默认删除最后一个

# dic = {}
# dic.fromkeys("123",[23]) # 批量添加键值对{"1":[23],"2":[23],"3":[23]}
# print(dic)

# dic = dict.fromkeys("123456789",1) # 批量添加键值对"键是可迭代对象",值 -- 会被共用
# dic["1"] = 18
# print(dic)

# set:
    # set() -- 空集合
    # {} -- 空字典
    # 定义集合:
    # set("alex")  # 迭代添加的

# bool: False
# 数字: 0
# 字符串: ""
# 列表:[]
# 元组:()
# 字典:{}
# 集合: set()
# 其他: None

# 数据类型之间转换

# list  tuple
# tuple list
# str list
# name = "alex"  print(name.split())
# list str
# lst = ["1","2","3"] # print(''.join(lst))

# dict -- str
# dic = {"1":2}
# print(str(dic),type(str(dic)))
# print(dict("{1:1}"))

# set - list
# list - set

# python数据类型:
# 可变:
# list ,dict ,set
# 不可变:
# int bool str tuple
# 有序:
# list,tuple,str,int,bool
# 无序:
# dict,set
# 取值方式:
# 索引: str list tuple
# 直接: set ,int ,bool
#   键: dict 
