# lst = [1,2]
# for i in lst:
#     lst.append(3)
# print(lst)  # 死循环

# 删除列表的坑

# lst = [1,2,3,4]
# for i in lst:
#     lst.pop()
# print(lst)

# lst = [1,2,3,4]
# for i in lst:
#     lst.pop(0)
# print(lst)

# lst = [1,2,3,4]
# for i in lst:
#     lst.remove(i)
# print(lst)

# 成功删除的方式:
# lst = [1,2,3,4,6]
# for i in range(len(lst)):
#     lst.pop()
# print(lst)

# lst = [1,2,3,4,6]
# for i in range(len(lst)-1,-1,-1):
#     del lst[i]
# print(lst)


# lst = [1,2,3,4,6]
# for i in range(len(lst)):
#     del lst[-1]
# print(lst)


# lst = [1,2,3,4,5,6]
# lst1 = lst.copy()
# for i in lst1:
#     lst.remove(i)
# print(lst)

# 删除字典的坑

# dic = dict.fromkeys("12345",1)  # 字典的迭代的时候改变了原来的大小(不能加不能删)
# for i in dic:
#     dic[i] = "123"
# print(dic)

# dic = dict.fromkeys("12345",1)
# dic1 = dic.copy()
# for i in dic1:
#     dic.pop(i)
# print(dic)

# 集合和字典都是迭代的时候不能改变原来的大小
