"""
7.使用for循环对s="321"进行循环，打印的内容依次是："倒计时3秒"，"倒计时2秒"，"倒计时1秒"，"出发！"
"""

# s = "321"
# for i in s:
#     print("倒计时%s秒"%i)
# print("出发")

# 字符串格式化


"""
9.选做题：实现一个整数加法计算器（多个数相加）：
如：content = input("请输入内容:") 用户输入：5+9+6 +12+ 13，然后进行分割再进行计算。
"""
# content = input("请输入内容:")
# ret_list = content.split("+")
# num = 0
# for i in ret_list:
#     num += int(i)
# print(num)

# content = input("请输入内容:")
# ret_list = content.split("+")
# num = 0
# for i in ret_list:
#     new_i = i.replace(" ","")
#     num += int(new_i)
# print(num)

"""
10.计算用户输入的内容中有几个整数（以个位数为单位）。
如：content = input("请输入内容：") # 如fhdal234slfh98769fjdla
"""
# int_num = 0
# content = input("请输入内容：")  # fhdal234slfh98769fjdla
# for i in content:
#     if i.isdecimal():
#         int_num += 1
# print(int_num)

# 前卫的想法
# int_num = 0
# content = input("请输入内容：")  # fhdal234slfh98769fjdla
# for i in content:
#     if type(i) == int:
#         int_num += 1
# print(int_num)

"""
11.写代码：计算 1 - 2 + 3 ... + 99 中除了88以外所有数的总和？
"""

# count = 1
# num_sum = 0
# while count < 100:
#     if count % 2 == 1:
#         # 奇数
#         num_sum += count
#     else:
#         #偶数
#         num_sum -= count
#     count += 1
# print(num_sum + 88)


"""
12.选做题：写代码，完成下列需求：
用户可持续输入（用while循环），用户使用的情况：
输入A，则显示走大路回家，然后在让用户进一步选择：
是选择公交车，还是步行？
选择公交车，显示10分钟到家，并退出整个程序。
选择步行，显示20分钟到家，并退出整个程序。
输入B，则显示走小路回家，并退出整个程序。
输入C，则显示绕道回家，然后在让用户进一步选择：
是选择游戏厅玩会，还是网吧？
选择游戏厅，则显示 ‘一个半小时到家，爸爸在家，拿棍等你。’并让其重新输入A，B,C选项。
选择网吧，则显示‘两个小时到家，妈妈已做好了战斗准备。’并让其重新输入A，B,C选项。
"""

"""
13.选做题：判断⼀句话是否是回⽂. 回⽂: 正着念和反着念是⼀样的. 例如, 
上海⾃来⽔来⾃海上(使用步长)
"""
# s = "alex"
# print(s[::-1])

# s = "上海⾃来⽔来⾃海上"
# if s == s[::-1]:
#     print("是回文")
# else:
#     print("不是回文")

# 面试题
# s = "给阿姨倒杯卡布奇诺"
# print(s[::-1])

