# 1.自定义模块
#     import 工具箱
#     from 工具箱 import 工具
#     from 工具箱 import *
#     __all__ = ["func","name"]  # 控制*获取的工具
      # 起别名 import from

      # 模块的两个功能:
          # if __name__ == '__main__':
          #     在当前模块中使用,if下边的代码会执行
          #     当模块被导入的时候 if下边的代码不会执行

      # 模块导入的坑:

      # 模块导入的路径:
      # 相对路径:
            # from 工具箱.工具箱 import 工具
      # 绝对路径:
      #     from sys import path
      #     path.insert(0,绝对路径)
      # 自定义 > 内置 > 第三方

# 2.time
    # time.time()   # 时间戳 浮点型
    # time.sleep()  # 睡眠 秒单位
    # time.gmtime() / time.localtime() # 时间戳 -- 结构化
    # time.strftime("格式化","结构化时间") #  结构化 -  字符串
    # time.strptime("字符串","格式化") # 字符串 - 结构化
    # time.mktime() # 结构化 -- 时间戳

# 3.datetime
    # 将当前时间转化成时间戳
    # t = datetime.now()
    # print(t.timestamp())

    # 将时间戳转化成当前时间
    # import time
    # print(datetime.fromtimestamp(15000000000))

    # 将字符串转成对象
    # print(type(datetime.strptime("2019-10-10 22:23:24","%Y-%m-%d %H:%M:%S")))

    # 将对象转成字符串
    # print(str(datetime.now()))
    # print(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


    # datetime加减
    # print(datetime.now() + timedelta(hours=30 * 24 * 12))
    # print(datetime.now() - timedelta(hours=30 * 24 * 12))

# 4.random (随机数)

    # print(random.random())          # 0 ~ 1
    # print(random.uniform(1,10))     # 1 ~ 10
    # print(random.randint(1,50))     # 1 ~ 50(闭区间)
    # print(random.randrange(1,5,2))    # randrange(起始,终止,步长)
    # print(random.choice([1,2,3,4,5,])) # 选择一个元素
    # print(random.choices([1,2,3,4,5,],k=2))   # 选择两个元素,会有重复
    # print(random.sample((1,2,3,4,5),k=2))  # 选择两个元素,不会有重复(除非只有两个)

    # lst = [1,2,3,4,5,6,7,8,9,0]
    # random.shuffle(lst)  # 顺序打乱
    # print(lst)
