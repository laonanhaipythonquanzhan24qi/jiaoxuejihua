# 1.编码
    # encode()  # 编码
    # decode()  # 解码
    # 用什么编码集进行编码就用什么编码集进行解码

# 2.基础数据类型补充
#     字符串:
#         首字母大写
#         每个单词首字母大写
#         大小写反转
#         查找 find -1 index 报错
#         join 拼接
#         居中 -- 填充
#         格式化 -- format 三种 1.位置顺序 2.索引值 3.关键字

#3.列表
    # 定义 list()
    # 排序 默认升序 -- sort(reverse=True)
    # 反转
    # + 合并
    # * 元素共用

# 4.元组
#    (1)  # 括号数据本身
#    (1,) # 元组
#     count  统计
#     len() 共用函数


# 5.集合
#     set() -- 空集合
#     set("alex") -- 迭代添加


# 6.字典
    # 随机删除 popitem python3.6以上 默认删除最后一个
    # fromkeys 批量添加

# 7.循环删除列表
#     1.从后向前删除
#     2.获取一个新的列表,循环新的列表,删除旧的列表

# 8.布尔值转换
#     数字：0
#     字符串： ""
#     list  []
#     tuple ()
#     dict {}
#     set  set()
#     None

# 9.字典(集合)循环删除
#     1.字典和集合在循环中不能改变源数据大小(不能删除和添加)

# 10.数据类型总结:
#     不可变: str,int,bool,tuple
#     可变: dict,list,set
#     有序: str,int,bool,list,tuple
#     无序: dict,set
#     取值方式:
#           1.直接取值:set,int,bool
#           2.索引取值: list,tuple,str
#           3.通过键取值: dict