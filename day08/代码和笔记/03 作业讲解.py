# import copy
# v1 = [1,2,3,{"name":'太白',"numbers":[7,77,88]},4,5]
# v2 = copy.deepcopy(v1)
# print(v1 is v2)  # False
# print(v1[0] is v2[0])  # True
# print(v1[3] is v2[3])  # False
#
# print(v1[3]['name'] is v2[3]['name']) # T
# print(v1[3]['numbers'] is v2[3]['numbers']) # F
# print(v1[3]['numbers'][1] is v2[3]['numbers'][1]) # T

# [['_','_','_'],['_','_','_'],['_','_','_']]

# lst = []
# for i in range(3):
#     lst.append(["_"] * 3)
# print(lst)

# dic = dict.fromkeys('abc',[])  # {"a":[666,111],"b":[666,111],"c":[666,111]}
# dic['a'].append(666)
# dic['b'].append(111)
# print(dic)

# 16.
# l1 = [11, 22, 33, 44, 55]，请把索引为奇数对应的元素删除（不能一个一个删除）
# l1 = [11, 22, 33, 44, 55]
# del l1[1::2]
# print(l1)

# for i in range(len(l1)-1,-1,-1):
#     if i % 2 == 1:
#         l1.pop(i)
# print(l1)

# 17.dic = {'k1':'太白','k2':'barry','k3': '白白', 'age': 18} 请将字典中所有键带k元素的键值对删除.

# dic = {'k1':'太白','k2':'barry','k3': '白白', 'age': 18}
# dic1 = dic.copy()
# for i in dic1:
#     if "k" in i:
#         dic.pop(i)
# print(dic)

"""
用户输入一个数字，判断一个数是否是水仙花数。 1**3 + 5**3 + 3**3 == 153
水仙花数是一个三位数, 三位数的每一位的三次方的和还等于这个数. 那这个数就是一个水仙花数,
例如: 153 = 1**3 + 5**3 + 3**3
"""
# num = input("请输入一个三位数:")
# num_sum = 0
# if num.isdecimal():
#     for i in num:
#         num_sum += int(i) ** 3
#     if int(num) == num_sum:
#         print("水仙花")
#     else:
#         print("不是水仙花")
# else:
#     print("请输入数字!")

"""
把列表中所有姓周的⼈的信息删掉(此题有坑, 请慎重):
lst = ['周⽼⼆', '周星星', '麻花藤', '周扒⽪']
结果: lst = ['麻花藤']
"""
# lst = ['周⽼⼆', '周星星', '麻花藤', '周扒⽪']
# for i in range(len(lst)-1,-1,-1):
#     if lst[i][0] == "周":
#         lst.pop(i)
# print(lst)


"""
21.车牌区域划分, 现给出以下车牌. 根据车牌的信息, 分析出各省的车牌持有量. (选做题)
cars = ['鲁A32444','鲁B12333','京B8989M','⿊C49678','⿊C46555','沪 B25041']
locals = {'沪':'上海', '⿊':'⿊⻰江', '鲁':'⼭东', '鄂':'湖北', '湘':'湖南'}
结果: {'⿊⻰江':2, '⼭东': 2, '上海': 1}
"""

# cars = ['鲁A32444','鲁B12333','京B8989M','⿊C49678','⿊C46555','沪 B25041']
# locals = {'沪':'上海', '⿊':'⿊⻰江', '鲁':'⼭东', '鄂':'湖北', '湘':'湖南'}
#
# dic = {}  # {}
# for i in cars:
#     key = i[0] # 获取车牌的第一个字
#     if key in locals:
#         new_key = locals[key] # 结果的键
#         dic[new_key] = dic.get(new_key,0) + 1
# print(dic)

