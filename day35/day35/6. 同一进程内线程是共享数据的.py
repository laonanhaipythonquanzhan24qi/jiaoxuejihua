# from threading import Thread
# import os
#
# x = 3
# def task():
#     global x
#     x = 100
#
# if __name__ == '__main__':
#
#     t1 = Thread(target=task)
#     t1.start()
#     t1.join()
#     print(f'===主线程{x}')

# 同一进程内的资源数据对于这个进程的多个线程来说是共享的.