学习编程语言的三个阶段

![1565227140408](C:\Users\Administrator\Desktop\assets\1565227140408.png)





1. 今日内容大纲

   1. 私有成员公有成员

   2. 实例方法

   3. 类方法

   4. 静态方法

   5. 属性

   6. issubclass isinstance区别

   7. 元类type的(了解)

       

2. 昨日内容回顾

   封装继承多态.

   封装: 

   多态: 一个事物呈现多种形态.

   鸭子类型: 

   类的约束: 

   ​	

   ```
   class Payment:
   	def pay(self):
   		raise Exception('错误')
   from abc import ABCmata, abstmethod
   class Payment(metclass=ABCmata):
   	@absmethod
   	def pay(self):
   		pass
   ```

   super(C,self): 严格按照对象从属于类的mro的顺序,查询下一个类. 

   

3. 今日内容

   1. 私有成员公有成员

      类的私有成员: 私有类的静态属性, 私有类的方法, 私有对象的属性

   2. 实例方法

   3. 类方法

   4. 静态方法

   5. 属性

   6. issubclass isinstance区别

   7. 元类type的(了解)

4. 今日总结

   私有成员: 只能在类的内部调用.

   类方法: @classmethod 类名  --->cls 实例化对象, 操作类的属性.

   静态方法: @staticmethod 保持代码的一致性,提高代码的维护性.

   属性: 将方法       伪装        成属性,看起来更合理.

   issbuclass(a,b): a 是否是b的子孙类.

   isinstance(a,b): a是否是 b类或者b类的派生类的实例化对象. 

5. 明日预习