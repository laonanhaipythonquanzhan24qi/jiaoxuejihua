import queue
# 第一种 先进先出
# q = queue.Queue(3)
# q.put(1)
# q.put(2)
# q.put(3)
# # q.put(4)
# print(q.get())
# print(q.get())
# print(q.get())
# # print(q.get(block=False))
# q.get(timeout=2)  # 阻塞2s 还没有值直接报错

# 第二种 后进先出 LiFo 堆栈
# q = queue.LifoQueue(4)
# q.put(1)
# q.put(2)
# q.put('alex')
# q.put('太白')
#
# print(q.get())
# print(q.get())
# print(q.get())
# print(q.get())

# 第三种优先级队列
q = queue.PriorityQueue(4)
q.put((5, '元宝'))
q.put((-2,'狗狗'))
q.put((0, '李业'))
# q.put((0, '刚哥'))
print(q.get())
print(q.get())
print(q.get())
















