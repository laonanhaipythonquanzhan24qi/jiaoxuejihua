# 版本一:
# 如果程序中的其他线程需要通过判断某个线程的状态来确定自己下一步的操作
# from threading import Thread
# from threading import current_thread
# import time
#
# flag = False
# def check():
#     print(f'{current_thread().name} 监测服务器是否开启...')
#     time.sleep(3)
#     global flag
#     flag = True
#     print('服务器已经开启...')
#
# def connect():
#     while 1:
#         print(f'{current_thread().name} 等待连接...')
#         time.sleep(0.5)
#         if flag:
#             print(f'{current_thread().name} 连接成功...')
#             break
#
# t1 = Thread(target=check,)
# t2 = Thread(target=connect,)
# t1.start()
# t2.start()

# 版本二: 事件event

# from threading import Thread
# from threading import current_thread
# from threading import Event
# import time
#
# event = Event()
# def check():
#     print(f'{current_thread().name} 监测服务器是否开启...')
#     time.sleep(3)
#     print(event.is_set())
#     event.set()
#     print(event.is_set())
#     print('服务器已经开启...')
#
# def connect():
#
#     print(f'{current_thread().name} 等待连接...')
#     # event.wait()  # 阻塞 直到 event.set() 方法之后
#     event.wait(1)  # 只阻塞1秒,1秒之后如果还没有进行set 直接进行下一步操作.
#     print(f'{current_thread().name} 连接成功...')
#
# t1 = Thread(target=check,)
# t2 = Thread(target=connect,)
# t1.start()
# t2.start()

# 一个线程监测服务器是否开始,
# 另个一线程判断如果开始了,则显示连接成功,此线程只尝试连接3次,1s 一次,如果超过3次,还没有连接成功,则显示连接失败.


# from threading import Thread
# from threading import current_thread
# from threading import Event
# import time
#
# event = Event()
# def check():
#     print(f'{current_thread().name} 监测服务器是否开启...')
#     time.sleep(4)
#     event.set()
#     print('服务器已经开启...')
#
# def connect():
#     count = 1
#     while not event.is_set():
#         if count == 4:
#             print('连接次数过多,已断开')
#             break
#         event.wait(1)
#         print(f'{current_thread().name} 尝试连接{count}次')
#         count += 1
#     else:
#         print(f'{current_thread().name} 连接成功...')
#
# t1 = Thread(target=check,)
# t2 = Thread(target=connect,)
# t1.start()
# t2.start()


