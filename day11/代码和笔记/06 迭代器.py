# 迭代:  器:工具

# 可迭代对象

# list,dict,str,set,tuple -- 可迭代对象  使用灵活

# 方式一:
# list.__iter__()
# dict.__iter__()
# str.__iter__()
# set.__iter__()
# tuple.__iter__()

# 方法二:
# 查看源码

# 方法三:
# print(dir(list))

# 官方声明只要具有__iter__()方法的就是可迭代对象

# 可迭代对象的优点:
    # 1.使用灵活
    # 2.直接查看值
# 可迭代对象的缺点:
#   1.消耗内存

# list,tuple,str -- 索引
# dict -- 键
# set  -- 直接取值

# 迭代器:
# 官方声明只要具有__iter__()方法__next__()方法就是迭代器
# f = open("xxxx","w")
# f.__iter__()
# f.__next__()

# lst = [1,2,3,4,6]
# new_list = lst.__iter__()  #将可迭代对象转换成迭代器
#
# new_list.__iter__()
# new_list.__next__()

# s = "123434"
# new_s = s.__iter__()       #将可迭代对象转换成迭代器
# print(new_s)
# new_s.__iter__()
# new_s.__next__()


# new_s = s.__iter__()       #将可迭代对象转换成迭代器
# print(new_s)
# new_s.__iter__()
# print(new_s.__next__())
# print(new_s.__next__())
# print(new_s.__next__())

# s = "12343"               # 更改版for的本质
# s = [1,2,3,4,5,7]
# count = len(s)
# new_s = s.__iter__()
# while count:
#     print(new_s.__next__())
#     count -= 1


# print(new_s.__next__())
# print(new_s.__next__())
# print(new_s.__next__())
# print(new_s.__next__())
# print(new_s.__next__())
# print(new_s.__next__())

# s = "12345"
# new_s = s.__iter__()
# while True:
#     try:
#         print(new_s.__next__())   # for真实本质
#     except StopIteration:
#         break


# except Exception:
#     print("我是万能的!")
#     break


# 总结:
#     可迭代对象:
#         优点:使用灵活,可以直接查看值
#         缺点:占内存,不能迭代取值
#
#     迭代器:
#         优点:节省内存,惰性机制
#         缺点:使用不灵活,操作比较繁琐,不能直接查看元素


# 迭代器的特性:
#     1.一次性的(用完就没有了)
#     2.不能逆行(不能后退)
#     3.惰性机制(节省内存)

# 什么是可迭代对象:
#     具有很多私有方法,具有__iter__()方法的就是一个可迭代对象

# 什么是迭代器:
#     具有__iter__()和__next__()方法的就是迭代器

# 迭代器什么时候使用:当容器中数据量较多的时候使用迭代器