# python3.6版本及以上才能够使用

# s = f"你好{'常鑫'}"       # 填充字符串
# s1 = F"你好{'常鑫'}"

# s1 = f"你还是挺好的{s}" # 填充变量
# print(s1)

# s1 = f"{35+15}"         # 填充计算公式
# print(s1)

# a = 10
# b = 20
# s1 = f"{a if a>b else b}"  # 填充表达式
# print(s1)

# s1 = f"{{{{{{'常鑫吃..'}}}}}}"  # 填充大括号
# print(s1)

# s1 = f"{{}}"
# print(s1)


# s1 = f"{'{}{}{}{}{}'}"
# print(s1)

# s1 = f"{print(123)}"
# print(s1)

# def prin(a):
#     print(a)
#
# s1 = f"{prin(123)}"
# prin(s1)

# def foo():
#     def func():
#         a = 1
#         return a
#     return func()
#
# s1 = f"{foo()}"
# print(s1)

# lst = [1,3,4,5,6]
# s1 = f"{lst[0:5]}"
# print(s1)  # [1,3,4,5,6]

# dic = {"key1":123,"key2":345}
# s1 = f"{dic['key2']}"
# print(s1)

# %s,format,f

