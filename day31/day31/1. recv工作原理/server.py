# 1，验证服务端缓冲区数据没有取完，又执行了recv执行，recv会继续取值。

# import socket
#
# phone =socket.socket(socket.AF_INET,socket.SOCK_STREAM)
#
# phone.bind(('127.0.0.1',8080))
#
# phone.listen(5)
#
# conn, client_addr = phone.accept()
# from_client_data1 = conn.recv(2)
# print(from_client_data1)
# from_client_data2 = conn.recv(2)
# print(from_client_data2)
# from_client_data3 = conn.recv(1)
# print(from_client_data3)
# conn.close()
# phone.close()

# 2，验证服务端缓冲区取完了，又执行了recv执行，此时客户端20秒内不关闭的前提下，recv处于阻塞状态。
#
# import socket
#
# phone =socket.socket(socket.AF_INET,socket.SOCK_STREAM)
#
# phone.bind(('127.0.0.1',8080))
#
# phone.listen(5)
#
# conn, client_addr = phone.accept()
# from_client_data = conn.recv(1024)
# print(from_client_data)
# print(111)
# conn.recv(1024) # 此时程序阻塞20秒左右，因为缓冲区的数据取完了，并且20秒内，客户端没有关闭。
# print(222)
#
# conn.close()
# phone.close()
#
#
# # 3 验证服务端缓冲区取完了，又执行了recv执行，此时客户端处于关闭状态，则recv会取到空字符串。
#
# import socket
#
# phone =socket.socket(socket.AF_INET,socket.SOCK_STREAM)
#
# phone.bind(('127.0.0.1',8080))
#
# phone.listen(5)
#
# conn, client_addr = phone.accept()
# from_client_data1 = conn.recv(1024)
# print(from_client_data1)
# from_client_data2 = conn.recv(1024)
# print(from_client_data2)
# from_client_data3 = conn.recv(1024)
# print(from_client_data3)
# conn.close()
# phone.close()

# recv空字符串: 对方客户端关闭了,且服务端的缓冲区没有数据了,我再recv取到空bytes.