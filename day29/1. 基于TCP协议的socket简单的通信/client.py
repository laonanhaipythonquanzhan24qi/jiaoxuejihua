import socket

# 买电话
phone = socket.socket(socket.AF_INET,socket.SOCK_STREAM)  # 默认基于TCP协议的socket

# 拨号打电话
phone.connect(('127.0.0.1',8848))
data = input('请输入>>>')

phone.send(data.encode('utf-8'))
from_server_data = phone.recv(1024)
print(f'来自服务端的消息:{from_server_data}')


# 关闭电话
phone.close()