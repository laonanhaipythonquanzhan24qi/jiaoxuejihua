# 今日内容



## HTTP协议   

```
http协议
请求信息格式
GET / HTTP/1.1   请求行
Host: 127.0.0.1:8003  请求头
Connection: keep-alive
Cache-Control: max-age=0
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3
Accept-Encoding: gzip, deflate, br
Accept-Language: zh-CN,zh;q=0.9
#空行
请求数据  username=ziwen password=666


get请求 请求数据部分是没有数据的,get请求的数据在url上,在请求行里面,有大小限制,常见的get请求方式: 浏览器输入网址,a标签
post请求 请求数据在请求体(请求数据部分) ,数据没有大小限制, 常见方式:form表单提交数据

```



MVC和MTV框架模式

```
MVC:
	M -- models 数据库相关
	V -- views  视图相关(逻辑)
	C -- controller url控制器(url分发器,路由分发)
django -- MTV
	M -- models 数据库相关
	T -- templates HTML相关 html就是模板
	V -- views  视图相关(逻辑)
	
	+ controller url控制器(url分发器,路由分发)
```

django下载安装

```
下载
	pip3 install django==1.11.9 
	pip3 install django==1.11.9 -i http://xxxxxx  指定源
创建项目
	django-admin startproject mysite   创建了一个名为"mysite"的Django 项目
启动项目
	python manage.py runserver  默认是127.0.0.1:8000
	python manage.py runserver 127.0.0.1  默认端口号是8000
    python manage.py runserver 127.0.0.1:8001 
	
```





django的url路由分发

```
    # url(r'^articles/(\d+)/(\d+)/', views.articles), #articles/2019/9/
	视图函数
		def articles(request,year,month):  # 位置参数 2019  9
            print(year,type(year)) #2019 <class 'str'>  #匹配出来的所有数据都是字符串
            print(month)

            return HttpResponse(year+'年'+ month +'月' +'所有文章')
		

    # 有名分组参数
    url(r'^articles/(?P<xx>\d+)/(?P<oo>\d+)/', views.articles), #articles/2019/9/
    #xx=2019  oo=9  关键字传参
	
    def articles(request,oo,xx):  # 关键字传参 2019  9
        print(xx,type(xx)) #2019 <class 'str'>  #匹配出来的所有数据都是字符串
        print(oo)
        return HttpResponse(xx+'年'+ oo +'月' +'所有文章')

```















