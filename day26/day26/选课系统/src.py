class Student:

    def __init__(self,name):
        self.name = name
        self.courses = []

    def show_courses(self):
        '''查看可选课程'''
        pass

    def select_course(self):
        '''选择课程'''
        pass

    def show_selected_course(self):
        '''查看所选课程'''
        pass

    def exit(self):
        '''退出'''
        pass

class Manager:

    # lst = [
    #     ('创建课程', 'create_course'),
    #     ('创建学生', 'create_student'),
    #     .......
    # ]
    def __init__(self,name):
        self.name = name

    def create_course(self):
        '''创建课程'''
        pass

    def create_student(self):
        '''创建学生'''
        pass

    def show_courses(self):
        '''查看可选课程'''
        pass

    def show_students(self):
        '''查看所有学生'''
        pass

    def show_students_courses(self):
        '''查看所有学生选课情况'''
        pass

    def exit(self):
        '''退出'''
        pass

class Course:
    def __init__(self,name,price,period):
        self.name = name
        self.price = price
        self.period = period
        self.teacher = None

# def login():
#     # 管理员登录成功
#     return {'username': 'admin','status':True, 'identy':"Manager"}
# import sys
# def main():
#     '''程序入口'''
#     ret = login()
#     if ret['status']:
#         if hasattr(sys.modules[__name__],ret['identy']):
#             obj = getattr(sys.modules[__name__],ret['identy'])(ret['username'])
#             for num,content in enumerate(obj.lst):
#                 print(f'{num+1} {content[0]}')
#             choose = 1
#
#
#     else:
#         pass
# main()
import pickle

obj1 = Course('python', 21800, '6个月')
obj2 = Course('linux', 20800, '6个月')

# with open('Course',mode='ab') as f1:
#     pickle.dump(obj1,f1)
# with open('Course', mode='ab') as f1:
#     pickle.dump(obj2,f1)

with open('Course',mode='rb') as f1:
    while 1:
        try:
            obj = pickle.load(f1)
            print(obj.name)

        except Exception:
            break