"""
当前py文件研究的是函数的内容回顾
"""
# open

# a,b,c = [1,2,3,4]
# print()

# a,*b,c = [1,2,3,4,5,6]
# print(a,b,c)

# a, *b = range(1,7)
# print(a,b)

# a, *c = (1,2,3,4)
# print(a,c)

# 形参角度的第四种传参方式: 仅限关键字参数
# def func(a,*args,sex='男',c, **kwargs):
#     print(a)
#     print(args)
#     print(sex)
#     print(c)
#     print(kwargs)
# func(1,c=4,name='alex')



# a = 2
# a = 4
# a = 100
#
# b = 3
#
# a = [1,2,3]
# c = [1,2,3]

# def func():
#     pass

# print(c)
# print(len)





# def wrapper(f):
#     def inner(*args,**kwargs):
#         ret = f(*args, **kwargs)
#         return ret
#     return inner
#
# @wrapper
# def func():
#     pass
#
# ret = wrapper(func)
# print(ret.__code__.co_freevars)

# def wrapper():
#     n = 1
#     def inner():
#         print(n)
#     # print(inner.__code__.co_freevars)
#     inner()
#     # return inner
# wrapper()

# ret = wrapper()
# print(ret.__code__.co_freevars)



# def func():
#     global n
#     n = 666
# # print(n)
# func()
# # print(n)
# print(globals())



