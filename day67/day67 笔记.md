# 昨日内容回顾

## form组件

```
1 生成html标签
2 保留原来的数据
3 校验用户提交的数据
```



```
class MyForm(forms.Form):
	name = forms.Charfield(
		required=True,
		max_length=16,
		min_length=8,
		label='书名',
		initial='xx',
		
		error_messages={
			'required':'不能为空',
			'min_length':'不能太短',
		}
		widget=forms.PasswordInput(attrs={'class':'c1',type:'password'})
		
	)

```





正则校验器

```
from django.core.validators import RegexValidator
 
class MyForm(Form):
    user = fields.CharField(
    	min_length=16,
        validators=[RegexValidator(r'^[0-9]+$', '请输入数字'), RegexValidator(r'^159[0-9]+$', '数字必须以159开头')],
    )
```



校验函数

```
 
# 自定义验证规则
def mobile_validate(value):
    mobile_re = re.compile(r'^(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$')
    if not mobile_re.match(value):
        raise ValidationError('手机号码格式错误')  #自定义验证规则的时候，如果不符合你的规则，需要自己发起错误

class MyForm(Form):
    user = fields.CharField(
        validators=[mobile_validate，],
    )
	
        
```



局部钩子

```

class LoginForm(forms.Form):

    username = forms.CharField(
        min_length=8,
        label="用户名",
        initial="张三",
        error_messages={
            "required": "不能为空",
            "invalid": "格式错误",
            "min_length": "用户名最短8位"
        },
        widget=forms.widgets.TextInput(attrs={"class": "form-control"})
    )
    ...
    # 定义局部钩子，用来校验username字段,之前的校验股则还在，给你提供了一个添加一些校验功能的钩子
    def clean_username(self):
        value = self.cleaned_data.get("username")
        if "666" in value:
            raise ValidationError("光喊666是不行的")
        else:
            return value
```

全局钩子

```
class LoginForm(forms.Form):
    ...
    password = forms.CharField(
        min_length=6,
        label="密码",
        widget=forms.widgets.PasswordInput(attrs={'class': 'form-control'}, render_value=True)
    )
    re_password = forms.CharField(
        min_length=6,
        label="确认密码",
        widget=forms.widgets.PasswordInput(attrs={'class': 'form-control'}, render_value=True)
    )
    ...
    def clean_password:
    	..
    def cleac_re_password()
    # 定义全局的钩子，用来校验密码和确认密码字段是否相同，执行全局钩子的时候，cleaned_data里面肯定是有了通过前面验证的所有数据
    def clean(self):
        password_value = self.cleaned_data.get('password')
        re_password_value = self.cleaned_data.get('re_password')
        if password_value == re_password_value:
            return self.cleaned_data #全局钩子要返回所有的数据
        else:
            self.add_error('re_password', '两次密码不一致') #在re_password这个字段的错误列表中加上一个错误，并且clean_data里面会自动清除这个re_password的值，所以打印clean_data的时候会看不到它
            #raise ValidationError('两次密码不一致')
```



大致流程

```
字段内部属性相关校验--局部钩子校验---然后循环下一个字段进行上面两步校验 -- 最后执行全局钩子
```



# 今日内容

## modelform

```
class BookModelForm(forms.ModelForm):
    # 优先级高
    # title = forms.CharField(
    #     label='书名2',
    # )

    class Meta:
        model = models.Book
        #fields='__all__'
	    fields=['title','publishs']	
        labels={
            'title':'书名',
            'price':'价格',
            'publishDate':'出版日期',
            'publishs':'出版社',
            'authors':'作者',
        }

        widgets={
            'publishDate':forms.TextInput(attrs={'type':'date'}),
            # 'publishDate2':forms.TextInput(attrs={'type':'date'}),
        }

        error_messages={
            'title':{'required':'书名不能为空！',},
            'price':{'required':'不能为空！',},
            'publishDate':{'required':'不能为空！',},
            'publishs':{'required':'不能为空！',},
            'authors':{'required':'不能为空！',},
        }


    #
    # def clean_title(self):
    #     pass
    # def clean(self):
    #     pass

    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        for field_name,field in self.fields.items():
            field.widget.attrs.update({'class':'form-control'})
```



## 同源和跨域

```
同源机制：域名、协议、端口号相同的同源

简单请求
（1) 请求方法是以下三种方法之一：（也就是说如果你的请求方法是什么put、delete等肯定是非简单请求）
    HEAD
    GET
    POST
（2）HTTP的头信息不超出以下几种字段：（如果比这些请求头多，那么一定是非简单请求）
    Accept
    Accept-Language
    Content-Language
    Last-Event-ID
    Content-Type：只限于三个值application/x-www-form-urlencoded、multipart/form-data、text/plain，也就是说，如果你发送的application/json格式的数据，那么肯定是非简单请求，vue的axios默认的请求体信息格式是json的，ajax默认是urlencoded的。
    
	#vue.js axios -- $.ajax
支持跨域，简单请求

　　　　服务器设置响应头：Access-Control-Allow-Origin = '域名' 或 '*'

　　支持跨域，复杂请求

　　　　由于复杂请求时，首先会发送“预检”请求，如果“预检”成功，则发送真实数据。

　　　　　　“预检”请求时，允许请求方式则需服务器设置响应头：Access-Control-Request-Method

　　　　　　“预检”请求时，允许请求头则需服务器设置响应头：Access-Control-Request-Headers
	res['Access-Control-Allow-Origin'] = 'http://127.0.0.1:8001'
    res['Access-Control-Allow-Headers'] = 'content-type'
    # res['Access-Control-Allow-Methods'] = 'PUT'
    # res['Access-Control-Allow-Origin'] = '*'
	
```



## Git 版本管理工具

```


```



集中式的版本管理工具SVN

分布式的版本管理工具Git 

```
1 安装git
2 创建一个文件夹 
3 在这个文件夹中执行下面的指令
	git init    初始化一个仓库  (git管理的本地仓库)
	git status  查看一下当前仓库中的文件或者文件夹的状态，新增的或者修改的都可以查看到
	git add 文件名  例如：git add index.html 单纯的管理这个文件
	git add .  管理所有的文件
	
	git commit -m '描述'  : git commit -m 'v1版本'  #git进行了版本管理
	
```









































