# 昨日内容回顾

## 视图相关

### request对象

```
request.path  
request.path_info
request.get_full_path()
request.META 
request.GET
request.POST
request.body
request.method
```

### response对象

```
HttpResponse
render
redirect
```

FBV(function based view) 和CBV(class based view)

```
from django.views import View

class LoginView(View):

	def dispatch()
		ret = super().dispatch()
		return ret
    def get(self,request):
    	return ...
	def post(self,request):
		return ...

url(r'^index/',views.LoginView.as_view())

```



### 装饰器

```


from django.utils.decorators import method_decorator

@method_decorator(装饰器名称,name='get')
class LoginView(View):
	@method_decorator(装饰器名称)
	def dispatch()
		ret = super().dispatch()
		return ret
	@method_decorator(装饰器名称)
    def get(self,request):
    	return ...
	def post(self,request):
		return ...

```



## 模板渲染

```
{{ 变量 }}  {% 逻辑 %}
```

### 万能的点

```
{{ name }}
{{ l1.1 }}
{{ l1.k1 }}
{{ l1.keys }}
{{ l1.values }}
{{ l1.items }}
{{ l1.p }}
```

### 过滤器

```
内置过滤器  |
{{ name|length }}
{{ name|cut:'xx' }}
default
filesizeformat
truncatchars
truncatwords
slice:'0:3'
join:'+'
date:'Y-m-d H:i:s'
safe  识别成标签

```

### 标签

```

{% for i in data %}
	xx
	{{ forloop.counter }}
{% empty %}
	xx
{% endfor %}


forloop.counter            当前循环的索引值(从1开始)，forloop是循环器，通过点来使用功能
forloop.counter0           当前循环的索引值（从0开始）
forloop.revcounter         当前循环的倒序索引值（从1开始）
forloop.revcounter0        当前循环的倒序索引值（从0开始）
forloop.first              当前循环是不是第一次循环（布尔值）
forloop.last               当前循环是不是最后一次循环（布尔值）
forloop.parentloop         本层循环的外层循环的对象，再通过上面的几个属性来显示外层循环的计数等
```



```
{% if a > 1  %}
	
{% elif %}
	
{% else %}
	
{% endif %}

```

```

{% with a=xx.xx.xx %}
{% with xx.xx.xx as a %}
	{{ a }}
{% endwith %}
```



# 今日内容

## 模板相关

### 模板继承(母版继承)

```
1. 创建一个xx.html页面(作为母版,其他页面来继承它使用)
2. 在母版中定义block块(可以定义多个,整个页面任意位置)
	{% block content %}  <!-- 预留的钩子,共其他需要继承它的html,自定义自己的内容 -->

	{% endblock %}


3 其他页面继承写法
	{% extends 'base.html' %}  必须放在页面开头
4 页面中写和母版中名字相同的block块,从而来显示自定义的内容
    {% block content %}  <!-- 预留的钩子,共其他需要继承它的html,自定义自己的内容 -->
        {{ block.super }}  #这是显示继承的母版中的content这个快中的内容
        这是xx1
    {% endblock %}

```



### 组件

```
1 创建html页面,里面写上自己封装的组件内容,xx.html
2 新的html页面使用这个组件
	{% include 'xx.html' %}
```

### 自定义标签和过滤器

```
1 在应用下创建一个叫做templatetags的文件夹(名称不能改),在里面创建一个py文件,例如xx.py

2 在xx.py文件中引用django提供的template类,写法
	from django import template
	register = template.Library() #register变量名称不能改
	
	定义过滤器
		@register.filter   参数至多两个
		def xx(v1,v2):
			return xxx
	使用:
		{% load xx %}
		{% name|xx:'oo' %}
	
	# 自定义标签 没有参数个数限制
    @register.simple_tag
    def huxtag(n1,n2):  #冯强xx  '牛欢喜'
        '''
        :param n1:  变量的值 管道前面的
        :param n2:  传的参数 管道后面的,如果不需要传参,就不要添加这个参数
        :return:
        '''
        return n1+n2

    # inclusion_tag 返回html片段的标签
    @register.inclusion_tag('result.html')
    def res(n1): #n1 : ['aa','bb','cc']

        return {'li':n1 }
	使用:
		{% res a %}
	
```



### 静态文件配置

```
1 项目目录下创建一个文件夹,例如名为jingtaiwenjianjia,将所有静态文件放到这个文件夹中
2 settings配置文件中进行下面的配置
	# 静态文件相关配置
    STATIC_URL = '/abc/'  #静态文件路径别名

    STATICFILES_DIRS = [
        os.path.join(BASE_DIR, 'jingtaiwenjianjia'),
    ]

3 引入<link rel="stylesheet" href="/abc/css/index.css">

```

### url别名和反向解析

```
写法
	url(r'^index2/', views.index,name='index'),
反向解析
	后端: from django.urls import reverse
		 reverse('别名')  例如:reverse('index') -- /index2/
	html: {% url '别名' %} -- 例如:{% url 'index' %} -- /index2/
```

### url命名空间

路由分发 include

```
1 在每个app下创建urls.py文件,写上自己app的路径
2 在项目目录下的urls.py文件中做一下路径分发,看下面内容
    from django.conf.urls import url,include
    from django.contrib import admin

    urlpatterns = [
        # url(r'^admin/', admin.site.urls),
        url(r'^app01/', include('app01.urls')),#app01/home/
        url(r'^app02/', include('app02.urls')),
    ]

```

命名空间namespace

```
from django.conf.urls import url,include
from django.contrib import admin
urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^app01/', include('app01.urls',namespace='app01')),#app01/home/
    url(r'^app02/', include('app02.urls',namespace='app02')),
	
]


使用:
	后端:reverse('命名空间名称:别名') -- reverse('app01:home') 
	hmtl:{% url '命名空间名称:别名' %}  -- {% url 'app01:home' %}
```























































































