# 昨日内容回顾

## modelform

```
class BookModelForm(forms.ModelForm):
    # 优先级高
    # title = forms.CharField(
    #     label='书名2',
    # )

    class Meta:
        model = models.Book
        #fields='__all__'
	    fields=['title','publishs']	
        labels={
            'title':'书名',
            'price':'价格',
            'publishDate':'出版日期',
            'publishs':'出版社',
            'authors':'作者',
        }

        widgets={
            'publishDate':forms.TextInput(attrs={'type':'date'}),
            # 'publishDate2':forms.TextInput(attrs={'type':'date'}),
        }

        error_messages={
            'title':{'required':'书名不能为空！',},
            'price':{'required':'不能为空！',},
            'publishDate':{'required':'不能为空！',},
            'publishs':{'required':'不能为空！',},
            'authors':{'required':'不能为空！',},
        }


    # def clean_title(self):
    #     pass
    # def clean(self):
    #     pass

    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        for field_name,field in self.fields.items():
            field.widget.attrs.update({'class':'form-control'})
```



## 同源和跨域

```
同源机制：域名、协议、端口号相同的同源

简单请求
（1) 请求方法是以下三种方法之一：（也就是说如果你的请求方法是什么put、delete等肯定是非简单请求）
    HEAD
    GET
    POST
（2）HTTP的头信息不超出以下几种字段：（如果比这些请求头多，那么一定是非简单请求）
    Accept
    Accept-Language
    Content-Language
    Last-Event-ID
    Content-Type：只限于三个值application/x-www-form-urlencoded、multipart/form-data、text/plain，也就是说，如果你发送的application/json格式的数据，那么肯定是非简单请求，vue的axios默认的请求体信息格式是json的，ajax默认是urlencoded的。
    
	#vue.js axios -- $.ajax
支持跨域，简单请求

　　　　服务器设置响应头：Access-Control-Allow-Origin = '域名' 或 '*'

　　支持跨域，复杂请求
　　　　由于复杂请求时，首先会发送“预检”请求，如果“预检”成功，则发送真实数据。
　　　　　　“预检”请求时，允许请求方式则需服务器设置响应头：Access-Control-Request-Method

　　　　　　“预检”请求时，允许请求头则需服务器设置响应头：Access-Control-Request-Headers
	res['Access-Control-Allow-Origin'] = 'http://127.0.0.1:8001'
    res['Access-Control-Allow-Headers'] = 'content-type'
    # res['Access-Control-Allow-Methods'] = 'PUT'
    # res['Access-Control-Allow-Origin'] = '*'
	
```



## Git 版本管理工具

```


```



集中式的版本管理工具SVN

分布式的版本管理工具Git 

```
1 安装git
2 创建一个文件夹 
3 在这个文件夹中执行下面的指令
	git init    初始化一个仓库  (git管理的本地仓库) 
	git status  查看一下当前仓库中的文件或者文件夹的状态，新增的或者修改的都可以查看到
	git add 文件名  例如：git add index.html 单纯的管理这个文件
	git add .  管理所有的文件
	
	git commit -m '描述'  : git commit -m 'v1版本'  #git进行了版本管理
	
```



# 今日内容

垃圾回收机制

```
引用计数为主，标记清除和分代回收为辅
```

注册登录



crm业务梳理



表关系介绍



git









































